<?php

include("classes/quote.php");
include("classes/image.php");

$route = $_GET['route'];
$action = $_GET['action'];

if ($route == 'quote') {
    switch ($action) {
        case "random":
            $quote = new quote();
            echo $quote->random();
            break;
        default:
            echo "";
            break;
    }
}

if ($route == 'image') {
    switch ($action) {
        case "random":
            $image = new image();
            echo $image->random();
            break;
        default:
            echo "";
            break;
    }
}
