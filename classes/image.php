<?php

class image
{
    private $images = [];

    function __construct()
    {
        if ($handle = opendir(__DIR__ . '/../images')) {
            while (false !== ($entry = readdir($handle))) {
                if (substr($entry, 0,1) != ".") {
                    $this->images[] = $entry;
                }
            }

            closedir($handle);
        }
    }

    /**
     * @return string
     */
    public function random()
    {
        $index = rand(0, count($this->images) - 1);

        return $this->images[$index];
    }
}
