<?php

class quote
{
    private $quotes = [];

    function __construct()
    {
        $file = file_get_contents(__DIR__ . '/../quotes.txt');
        $this->quotes = explode("\n", $file);
    }

    /**
     * @return string
     */
    public function random()
    {
        $index = rand(0, count($this->quotes) - 1);
        return $this->quotes[$index];
    }
}
