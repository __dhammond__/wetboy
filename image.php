<?php
include("classes/image.php");

$image = new image();
$file_out = 'images/' . $image->random();

$image_info = getimagesize($file_out);

$fp = fopen($file_out, 'rb');
header('Content-Type: ' . $image_info['mime']);
header("Content-Length: " . filesize($file_out));
header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');
fpassthru($fp);